from random import randint

#ask for name

user_name = input("Hi! What is your name? ")


#define guessing rules
guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2004)

#loop this???
for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)
    print("Guess", guess_number, ": Were you born in",
       month_number, "/", year_number, "? ")
    response = input("yes or no? ")
    if response == "yes":
        print("I knew it!")
        break
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")

#--everything below is example from first attempt--
#begin guess #1

#print("Guess 1: ", user_name, "were you born in",
#    month_number, "/", year_number,"?")
#response = input("yes or no? ")
#if response == "yes":
#    print("I knew it!")
#    exit()
#else:
#    print("Drat! Lemme try again!")

#guess #5

#month_number = randint(1, 12)
#year_number = randint(1924, 2004)
#print("Guess 5: ", user_name, "were you born in",
#    month_number, "/", year_number,"?")
#response = input("yes or no? ")
#if response == "yes":
#    print("I knew it!")
#    exit()
#give up
#else:
#    print("I have other things to do. Good bye.")
#    exit()